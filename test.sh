while IFS= read -r var
do
    (>&2 echo "$var")
    time ./tspant -d 'datasets/'$var -c $2 -a $3 --sync
    time ./tspant -d 'datasets/'$var -c $2 -a $3 --async
done < $1 2> results.txt
