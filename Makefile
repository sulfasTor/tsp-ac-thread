SOURCES = $(wildcard src/*.cpp)
HEADERS = $(wildcard src/includes/*.hpp)

OBJECTS = $(SOURCES:%.cpp=%.o)
PROGRAM = tspant

CC := $(shell which gcc || which clang)
CFLAGS = -g -Wall -Wextra -Werror -std=c++17 -O3
LIBS = stdc++ m pthread
LDFLAGS = $(LIBS:%=-l%)

DOXYGEN := $(shell which doxygen)

$(PROGRAM) : $(OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $<

%.o : %.cpp $(HEADERS)
	$(CC) $(CFLAGS) -c -o $@ $<

doc:
	$(DOXYGEN) docs/Doxyfile

.PHONY : clean
clean :
	rm -f $(PROGRAM) $(OBJECTS)
	rm -rf docs/html docs/latex
