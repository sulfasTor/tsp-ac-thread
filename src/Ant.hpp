/**
 * \class Ant
 *
 * \brief Provides an ant from the Ant System Optimization model
 *
 * This class includes also the Path class. It describes the ant model in the ACO algorithm
 * This implies an artificial ant is:
 * - Artificial ants will have some memory (tabu list)
 * - they will not be completely blind
 * - they will live in an environment where time is discrete
 * - transitions to already visited towns are disallowed until a tour is
 *  completed (this is controlled by a tabu list) when it completes a
 *   tour.
 * - it lays a substance called trail on each edge (i,j) visited.
 *
 * \author Moises TORRES AGUILAR
 *
 * \version $Revision: 1.0 $
 *
 * \date $Date: 2019/04/05 14:16:20 $
 *
 * Contact: torresmoisesa@gmail.com
 *
 */
#ifndef ANT_H_
#define ANT_H_

#include "Colony.hpp"
 
#include <vector>

class Ant {
public:

  /// Create an Ant
  Ant(Colony& colony, int n_rooms, int start) : _colony(colony) {
    _start = start;
    _n_rooms = n_rooms;
  }
  
  /**
   * \brief Computes the tour cost
   *
   * Sums distances from paths saved in the tabu table
   *
   * @param void.
   * @return tour cost
   */
  double compute_tour_cost() {
    double l = 0;
    for (const Path& path : _tabu)
      l += path.distance();
    return l;
  }

  /**
   * \brief Completes a tour
   *
   * Completes a tour using Ant System Algorithm
   *
   * @param void.
   * @return this ant
   */
  Ant explore(bool async) {
    std::vector<Path> candidates;

    while (_time <= _n_rooms) {
      candidates = get_available_paths();
      if (candidates.empty())
    	break;
      visit_path(choose_next_path(candidates), async);
    }
    return *this;
  }
  /**
   * \brief Prints the current tabu table
   * 
   * Prints the current path saved in the tabu table
   *
   * @param void.
   * @return void
   */  
  void print_path() {
    for (Path& path : _tabu)
      path.print_path();
  }
  
  /**
   * \brief Gets visited rooms
   * 
   * Returns the visited rooms
   *
   * @param void
   * @return vector of int containing the visited rooms
   */
  std::vector<int> get_path() {
    std::vector<int> vec;
    vec.push_back(_start);
    for (Path& path : _tabu)
      vec.push_back(path.destination());
    return vec;
  }

private:
  int				_start;			//! Initial room
  int				_time = 1;		//! Discrete time
  int				_n_rooms;		//! Number of rooms to explore
  Colony&			_colony;		//! Colony
  std::vector<Path>		_tabu;			//! Visited path  
  
  /**
   * \brief Setter of _tabu
   * 
   * Returns tabu list
   *
   * @param path to visit
   * @return vector of Path containing the visited paths
   */
  void visit_path(Path& path, bool async) {
    _tabu.push_back(path);
    if (async)
      _colony.visit_path_mutex(path.index());
    else
      _colony.paths()[path.index()].visitors() += 1;
    _time++;
  }
  
  /**
   * \brief Checks if it is a valid move
   * 
   * Returns true if it is valid
   *
   * @param dest destination
   * @return vector of Path containing the visited paths
   */  
  bool valid_move(int dest) {
    for (Path& path : _tabu) {
      if (dest == path.destination() || (dest == _start && _time < _n_rooms))
    	return false;
    }
    return true;
  }
  
  /**
   * \brief Returns candidate paths
   * 
   * Search for unvisited paths
   *
   * @param void
   * @return vector of paths containing candidates
   */    
  std::vector<Path> get_available_paths() {
    std::vector<Path> paths;
    int current;

    if (_tabu.empty()) {
      current = _start;
    } else {
      auto last_path = _tabu.back();
      current = last_path.destination();
    }
    for (Path& path : _colony.paths()) {
      if (path.origin() == current && valid_move(path.destination())) {
	paths.push_back(path);
      }
    }
    return paths;
  }
  
  /**
   * \brief Return the chosen path
   * 
   * Choose the next path according to the ACO Algorithm
   *
   * @param vector of paths
   * @return Path to visit
   */      
  Path& choose_next_path(std::vector<Path>& paths) {
    float prob;
    float random;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0.0, 1.0001);

    for (Path& path : paths) {
      prob =  path.pheromone() / (float) path.distance();
      random = dis(gen);
      // printf("%f\n", random - prob);
      if (random - prob <= 0.0)
    	return path;
    }
    return paths.back();
  }
  
}; // class Ant

#endif // ANT_H
