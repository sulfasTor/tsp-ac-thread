#include "AntSystem.hpp"
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <getopt.h>

static int paralel_flag; //! Parallelisation flag

void print_usage() {
  std::cout << "Usage: tspant [-d FILE -a NUMBER -c NUMBER] [--async | --sync]" << std::endl;
  std::cout << "\t -d, --dataset \t\t\t TSP dataset " << std::endl;
  std::cout << "\t -a, --ants \t\t\t number of ants to place by room " << std::endl;
  std::cout << "\t -c, --cycle \t\t\t colony life cycles " << std::endl;
  std::cout << "\t     --sync \t\t\t Synchronous execution" << std::endl;
  std::cout << "\t     --async \t\t\t Asynchronous execution using threads" << std::endl;
}

int main (int argc, char ** const argv) {
  // Code boworred from https://www.gnu.org/software/libc/manual/html_node/Getopt-Long-Option-Example.html#Getopt-Long-Option-Example

  int c;
  std::string filename;
  int cycles = -1;
  int ants = -1;
  paralel_flag = 0;

  while (1)
    {
      static struct option long_options[] =
        {
	 {"async", no_argument, &paralel_flag, 1},
	 {"sync", no_argument, &paralel_flag, 0},
	 {"dataset", required_argument, 0, 'd'},
	 {"ants",    required_argument, 0, 'a'},
	 {"cycles",  required_argument, 0, 'c'},
	 {0, 0, 0, 0}
        };
      
      int option_index = 0;

      c = getopt_long (argc, argv, "a:c:d:",
                       long_options, &option_index);

      if (c == -1)
        break;

      switch (c)
        {
        case 0:
          /* If this option set a flag, do nothing else now. */
          if (long_options[option_index].flag != 0)
            break;
	  break;
	  
        case 'd':
	  filename = optarg;
          break;

	case 'a':
	  ants = std::strtoul(optarg, NULL, 0);
	  break;

	case 'c':
	  cycles = std::strtoul(optarg, NULL, 0);
	  break;

        default:
	  print_usage();
	  exit(1);
        }
    }

  if (filename.empty() || ants <=0 || cycles <= 0) {
    print_usage();
    exit(1);
  }

  // Dataset parsing
  std::ifstream data(filename, std::ios::binary);
  std::string line;
  std::vector<std::vector<uint>> vec;
  if (!data.is_open()) {
    std::cout << "Error: failed to open " << filename << "." << '\n';
    return 1;
  } else {
    while (std::getline(data, line)) {
      if (line.empty())
	continue;
      std::stringstream ss(line);
      std::vector<uint> tokens((std::istream_iterator<uint>(ss)), 
			       std::istream_iterator<uint>());
      vec.push_back(tokens);
    }
  }
  data.close();
  
  // Resolution
  auto as = AntSystem(vec, ants, cycles);
  if (paralel_flag)
    as.async_find_best_path();
  else
    as.sync_find_best_path();
  as.print_best_path();
  std::cout << " = " << as.best_cost() << "\n";  
 
  return 0;
}
