/**
 * \class Path
 *
 * \brief Provides a class who describes a transition path
 *
 * Used by the Colony class
 *
 * \author Moises TORRES AGUILAR
 *
 * \version $Revision: 1.0 $
 *
 * \date $Date: 2019/04/05 14:16:20 $
 *
 * Contact: torresmoisesa@gmail.com
 *
 */

#ifndef PATH_H_
#define PATH_H_

#include <iostream>
#include <random>

class Path {
public:
  
  Path(int index, int origin, int destination, double dist) {
    _index = index;
    _origin = origin;
    _destination = destination;
    _distance = dist;
    _pheromone = random_pheromone();
  }

  void update_pheromone_trail(int n_ants) {
    _pheromone = _pheromone / 2.0f + n_ants * _visitors;
    _visitors = 0;
  }

  // Getters and Setters
  
  const	double&		distance() const { return _distance; }
  const int&		origin() const { return _origin; }
  const int&		destination() const { return _destination; }
  float&		pheromone() { return _pheromone; }
  const float&		pheromone() const { return _pheromone;}
  const int&		index() const { return _index; }
  int&			visitors() { return _visitors; }
  
  void print_path() {
    std::cout << "(" << _origin << ")" << "--" << _distance << "-->(" << _destination << ")"; 
  }

  float random_pheromone() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0.0, 1.0001);

    return dis(gen);
  }
  
private:
  int		_index;
  double	_distance;
  float		_pheromone;
  int		_origin;
  int		_destination;
  int		_visitors = 0;

}; // class Path

#endif // PATH_H
