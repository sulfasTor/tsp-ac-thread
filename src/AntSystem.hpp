/**
 * \class AntSystem
 *
 * \brief Provides a class who implements the Ant System Optimization Algorithm.
 *
 * 
 * \author Moises TORRES AGUILAR
 *
 * \version $Revision: 1.0 $
 *
 * \date $Date: 2019/04/05 14:16:20 $
 *
 * Contact: torresmoisesa@gmail.com
 *
 */

#ifndef ANTSYSTEM_H_
#define ANTSYSTEM_H_

#include "Colony.hpp"
#include "Ant.hpp"

#include <vector>
#include <iostream>
#include <float.h>
#include <future>
#include <thread>

class AntSystem {
public:
  //! Receives an adjacent matrix
  AntSystem(const std::vector<std::vector<uint>>& graph, int n_ants, int nc) {
    _colony.initialize_colony(graph);
    _n_ants = n_ants;
    _n_rooms = (int) graph.size();
    _ants = place_ants(_colony, graph.size(), n_ants);
    _NC = nc;
  }

  void print_best_path() {
    for (int room : _best)
      std::cout << room << "-->";
  }
  
  void sync_find_best_path() {
    double cost;
    int i;

    for (i = 0; i < _NC; i++) {
      for (Ant& ant : _ants) {
	ant.explore(false);
	cost = ant.compute_tour_cost();
	if (cost < _best_cost) {
	  _best_cost = cost;
	  _best = ant.get_path();
	}
      }

      if (i < _NC - 1) {
	_colony.update_pheromone_trail(_n_ants);
	_ants = place_ants(_colony, _n_rooms, _n_ants);
      }
    }
  }
  
  void async_find_best_path() {
    double cost;
    int i;

    for (i = 0; i < _NC; i++) {
      std::vector<std::thread> threads;

      for (Ant& ant : _ants)
  	threads.push_back(std::thread(&Ant::explore, &ant, true));
     
      for (auto& th : threads)
  	th.join();

      for (Ant& ant : _ants) {
      	cost = ant.compute_tour_cost();
      	if (cost < _best_cost) {
      	  _best_cost = cost;
      	  _best = ant.get_path();
      	}
      }
      if (i < _NC - 1) {
  	_colony.update_pheromone_trail(_n_ants);
  	_ants = place_ants(_colony, _n_rooms, _n_ants);
      }
    }
  }
  
  const double& best_cost() const { return _best_cost; };
  
private:
  int				_n_ants = 0;                    //! Ants per room
  int				_n_rooms = 0;                   //! Number of rooms
  int				_NC = 0;                        //! Maximum number of cycles
  double			_best_cost = DBL_MAX;           //! Best cost

  Colony			_colony;                        //! Colony
  std::vector<Ant>		_ants;                          //! Ants
  std::vector<int>		_best;                          //! Best path
  
  std::vector<Ant> place_ants(Colony& colony, int n_rooms, int n_ants) {
    std::vector<Ant> ants;
    int i, j;

    for (i = 0; i < n_rooms; i++)
      for (j = 0; j < n_ants; j++)
	ants.push_back(Ant(colony, n_rooms, i));
    return ants;
  }
};

#endif // ANTSYSTEM_H
