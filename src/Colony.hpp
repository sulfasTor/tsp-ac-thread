/**
 * \class Colony
 *
 * \brief Provides a thread safe colony description
 *
 * This class includes also the Path class. It describes the ant colony in the ACO algorithm
 *
 * \author Moises TORRES AGUILAR
 *
 * \version $Revision: 1.0 $
 *
 * \date $Date: 2019/04/05 14:16:20 $
 *
 * Contact: torresmoisesa@gmail.com
 *
 */

#ifndef COLONY_H_
#define COLONY_H_

#include "Path.hpp"

#include <vector>
#include <mutex>
#include <random>
#include <algorithm>

class Colony {
public:

  void initialize_colony(const std::vector<std::vector<uint>>& gph) {
    int i, j, id;
    
    id = 0;
    for (i = 0; i < (int) gph.size(); i++)
      for (j = 0; j < (int) gph[0].size(); j++)
	if (gph[i][j] != 0)
	  _paths.push_back(Path(id++, i, j, gph[i][j]));
  }
  
  void print_colony() {
    for (Path &path : _paths)
      std::cout << "[" << path.origin() << "->" << path.destination() << "] : " \
		<< path.distance() << " : " << path.pheromone() << " : " << path.visitors() << "\n";
  }

  void update_pheromone_trail(int ants) {
    for (Path& path : _paths)
      path.update_pheromone_trail(ants);
  }

  void visit_path_mutex(int id) {
    std::lock_guard<std::mutex> lock(_mut);
    _paths[id].visitors() += 1;
  }
  
  std::vector<Path>& paths() { return _paths; }

private:
  std::mutex		_mut;			       //! Path visitors mutex
  std::vector<Path>	_paths;                        //! Colony paths

}; // Class Colony

#endif // COLONY_H
