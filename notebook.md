
# Table of Contents

1.  [Introduction](#org61c50a9)
2.  [Problème: Voyageur de commerce](#org0869c3a)
3.  [Algorithme évolutionnaire: Colonie de fourmis](#orgb9c4d82)
    1.  [Fourmis artificielles](#org052b55a)
    2.  [Phéromone](#orge309ca6)
    3.  [Cycle](#org3e70c2d)
    4.  [Algorithme](#org7cd46f6)
4.  [Parallélisation](#orgd56f95d)
    1.  [Comparaison avec std::async, std::thread et sans fil](#org3404eb3)
        1.  [Performance](#org1acc149)
5.  [Programme TSPANT](#org82eacbe)
    1.  [Installation](#org28b1797)
    2.  [Utilisation](#orgc21abe1)
    3.  [Approximation de la solution](#orgb58ccfb)
6.  [Conclusion](#org4e0eac8)
7.  [Datasets](#org60beed7)
    1.  [Données](#org8f1fbd0)
    2.  [Solutions](#orge02c716)



<a id="org61c50a9"></a>

# Introduction

Le but de ce projet est de résoudre de manière approchée le problème
du voyageur de commerce en utilisant un algorithme de Colonie de
fourmis et en effectuant des calculs parallèls. La solution est
présentée sous forme d'un programme écrit en langage C++ et
utilisant des libraires jusqu'à la norme C++17.

\newpage


<a id="org0869c3a"></a>

# Problème: Voyageur de commerce

Le problème du voyageur de commerce (PVC) est un problème
d'optimisation combinatoire consistant à trouver à partir d'une
liste de villes et de distances le plus court chemin passant une
fois par ville et arrivant au point de départ. Il s'agit d'un
problème NP-complet, c'est à dire qu'on ne sait pas trouver de
solution efficacement, donc on peut trouver une solution approchée
avec un degré d'aproximation dû à l'algorithme utilisé.


<a id="orgb9c4d82"></a>

# Algorithme évolutionnaire: Colonie de fourmis

L'algorithme de colonies de fourmis est un algorithme évolutionnaire
inspiré du comportement réel des fourmis. Il a été presenté par
l'informaticien Marco Dorigo dans sa thèse de doctorat en 1992.

Le comportement des fourmis est utile pour résoudre de manière
approché le probleme du PVC car les fourmis se déplacent presque
aléatoirement dans son milieu dans la recherche de
nourriture. Lorsqu'une fourmi trouve de nourriture elle essaiera de
rentrer le plus vite possible dans son nid en dégageant une
substance qui attira les fourmis voisines. De même ses fourmis
construiront un chemin de phéromones qui sera d'autant plus direct
au nid que du nombre de fourmis empruntant ce trajet. Pour ce projet
il a été utilisé l'algorithme de colonie des fourmis
artificielle. Il ajoute quelques caractéristiques au fourmis qui ne
correspondent pas forcément aux fourmis réelles.


<a id="org052b55a"></a>

## Fourmis artificielles

-   Elles ont de la mémoire qui leur permet de savoir quelles villes ont

été déjà visités (liste "tabou").

-   Elles ne visiteront pas une ville déjà visitée jusqu'à un tour soit
    complété grâce à la liste "tabou".
-   Elles habitent dans un espace temporel discret.
-   Elles dégagent une substance dans chaque transition entre villes.


<a id="orge309ca6"></a>

## Phéromone

C'est une substance attractive qui est dégagée par une fourmi après
une transition entre villes. Après chaque tour les phéromones sont
mises à jour selon des règles spécifiques. Un facteur d'évaporation
peu exister qui réduira la phéromone existante comme il arrive dans
la nature.


<a id="org3e70c2d"></a>

## Cycle

Un cycle est le temps pendant lequel toutes les fourmis finissent
un tour. Après chaque cycle les fourmis seront réinitialisés (liste
"tabou" mise à zéro) et les traces de phéromone seront mises à
jours pour chaque transition entre ville. Un cycle "tmax" sera
indiqué pour calculer la solution.


<a id="org7cd46f6"></a>

## Algorithme

L'algorithme utilisé dans ce projet est décrit ci-dessous

     1  
     2  Règle 1: Aller vers la ville (i,j) avec une probabilité p_ij/d_ij et si elle
     3    n'existe pas dans la liste "tabou".
     4  
     5  Règle 2: p_ij(t+s) = p_ij/2(t) + (nb de fourmis par ville) * (nb de fourmis
     6    passées entre i et j).
     7  
     8  Initialisation : Placer k fourmis par ville.
     9  Pour t = 1 à t = tmax faire
    10      pour chaque fourmi k faire
    11  	Construire un chemin Tk (t) avec la règle de transition (1).
    12  	Calculer la longueur Lk (t) de ce chemin.
    13      finpour
    14  Soient T+ le meilleur chemin trouvé et L+ la longueur correspondante.
    15  Mettre à jour les traces de phéromones suivant la règle (2).
    16  finpour
    17  Retourner T+ et L+.


<a id="orgd56f95d"></a>

# Parallélisation

Le but de ce projet étant de gagner du temps de calcul avec la
parallélisation, voici le justificatif de l'implémentation:

Il est naturel de paralléliser le calcul pour chaque fourmis car
chaque fourmi est indépendante l'une de l'autre. En plus, les traces
de phéromones ne sont mises à jour que lorsque toutes les fourmis
ont fini leur tour. (Algorithme L 14)

Dans cet implémentation à chaque fourmi correspond un fil. Donc le
nombre de fourmis est limité par le nombre de threads maximum de
l'ordinateur. 

Normalement ne devrait poser problème aucun évenèment de "data race"
car chaque fourmi effectue son tour en lisant les données de la
colonie et il n'existe pas d'écriture. Cependant dans ce projet on a
ajouté le fait qu'à une classe "Path", qui décrit une transition
entre villes, une fourmi peut incrémenter un attribut "visitors" qui
garde le nombre de fourmis l'ayant visité. 

Aussi une classe "Colony" décrit une liste d'instances de
"Paths". Une seule instance de "Colony" existe et elle est passée en
référence à toutes les fourmis qui pourront lire les éléments de
"Path" de cette "Colony". Puisque l'attribut "visitors" de "Path"
n'est pas lu par des "threads" il n'est pas nécessaire de bloquer la
lecture avec une mutuelle d'exclusion. Mais pour écrire dans
"Colony" il faut strictement avoir un accés unique à l'attribut
"visitors". C'est pour cela qu'un "mutex" est définit dans
"Colony". La méthode "visit\_path\_mutex" `(Colony.hpp:52)` bloquera
ce "mutex" à chaque écriture.

Le choix logique était donc de choisir `std::mutex` de `<mutex>`
puisque il existait un seule écrivain et pas de lecteur. Aussi
`std::lock_guard` a été choisit au lieu de `std::unique_lock`
puisque le temps de blocage ne durerait que pendant l'execution de
la méthode `visit_path_mutex(int)`.

    52  void visit_path_mutex(int id) {
    53    std::lock_guard<std::mutex> lock(_mut);
    54    _paths[id].visitors() += 1;
    55  }


<a id="org3404eb3"></a>

## Comparaison avec std::async, std::thread et sans fil

Pour la parallélisation il a été considérait d'utiliser
`std::thread` et `std::async`. Ce dernier servirait à récuperer le
résultat du tour le la fourmi mais le premier marchait aussi car on
avait mémorisait chaque fourmi dans une liste et donc il fallait de
la parcourir après l'execution des "threads". Pour choisir quelques
test ont été effectués et le temps d'execution a été mesuré.


<a id="org1acc149"></a>

### Performance

Voici quelques résultats mesurés avec la commande `time`. (real
time, `Intel(R) Core(TM) i5-3320M CPU @ 2.60GHz`).

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
<caption class="t-above"><span class="table-number">Table 1:</span> Performance</caption>

<colgroup>
<col  class="org-right" />

<col  class="org-right" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">cycles</th>
<th scope="col" class="org-right">fourmis</th>
<th scope="col" class="org-left">dataset</th>
<th scope="col" class="org-left">std::thread</th>
<th scope="col" class="org-left">std::async</th>
<th scope="col" class="org-left">sans thread</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">100</td>
<td class="org-right">100</td>
<td class="org-left">berlin52.tsp</td>
<td class="org-left">1m43,505s</td>
<td class="org-left">1m8,753s</td>
<td class="org-left">1m40,900s</td>
</tr>


<tr>
<td class="org-right">100</td>
<td class="org-right">55</td>
<td class="org-left">dantzig42\_d.txt</td>
<td class="org-left">0m48,872s</td>
<td class="org-left">0m48,851s</td>
<td class="org-left">1m3,871s</td>
</tr>


<tr>
<td class="org-right">10</td>
<td class="org-right">100</td>
<td class="org-left">fri26\_d.txt</td>
<td class="org-left">0m16,073s</td>
<td class="org-left">0m16,281s</td>
<td class="org-left">0m17,923s</td>
</tr>


<tr>
<td class="org-right">5</td>
<td class="org-right">400</td>
<td class="org-left">fri26\_d.txt</td>
<td class="org-left">0m6,506s</td>
<td class="org-left">0m6,616s</td>
<td class="org-left">0m7,226s</td>
</tr>


<tr>
<td class="org-right">5</td>
<td class="org-right">700</td>
<td class="org-left">fri26\_d.txt</td>
<td class="org-left">0m11,256s</td>
<td class="org-left">0m11,482s</td>
<td class="org-left">0m12,664s</td>
</tr>


<tr>
<td class="org-right">5</td>
<td class="org-right">900</td>
<td class="org-left">fri26\_d.txt</td>
<td class="org-left">0m14,467s</td>
<td class="org-left">0m14,853s</td>
<td class="org-left">0m16,222s</td>
</tr>


<tr>
<td class="org-right">500</td>
<td class="org-right">3</td>
<td class="org-left">fri26\_d.txt</td>
<td class="org-left">0m5,008s</td>
<td class="org-left">0m5,041s</td>
<td class="org-left">0m5,529s</td>
</tr>


<tr>
<td class="org-right">500</td>
<td class="org-right">10</td>
<td class="org-left">fri26\_d.txt</td>
<td class="org-left">0m16,349s</td>
<td class="org-left">0m17,558s</td>
<td class="org-left">0m18,000s</td>
</tr>
</tbody>
</table>

On remarque que selon les paramètres `std::async` peut être mieux que
`std::thread` mais la différence n'est pas importante. Lorsque il y a
peu de cycles et plus fourmis `std::thread` présente une petit
avantage sur `std::async`. Aussi l'utilisation de `std::future` peut
être plus lente, en gardant les fourmis dans une liste cela nous
permet de l'éviter.


<a id="org82eacbe"></a>

# Programme TSPANT


<a id="org28b1797"></a>

## Installation

Le code source est disponible dans le git:
<https://gitlab.com/sulfasTor/tsp-ac-thread.git>. Le seul pré-requis
est d'avoir un compilateur c++17. Pour compiler taper `make`. Pour
compiler la documentation, taper `make doc`.


<a id="orgc21abe1"></a>

## Utilisation

Le programme `tspant` permet de recevoir des paramètres depuis le
terminal grâce à `getopt`.

    Usage: tspant [-d FILE -a NUMBER -c NUMBER] [--async | --sync]
    	 -d, --dataset 			 TSP dataset 
    	 -a, --ants 			 number of ants to place by room 
    	 -c, --cycle 			 colony life cycles 
    	     --sync 			 Synchronous execution
    	     --async 			 Asynchronous execution using threads

Pour regarde le code, veuillez générer la documentation avec
Doxygen.

Le "dataset" est une fichier contenant une matrice de distances. En ce
moment c'est le seul format accepté. Quelques exemples sont fournis
dans le projet. 

L'option `--async` permet de calculer les tour des fourmis en
parallèl. Par défaut le programme s'execute en mode `--sync` qui
n'utilise pas les "threads".

Voici une comparaison des temps d'execution:

-   `cycles = 10, fourmis = 10`

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">dataset</th>
<th scope="col" class="org-right">taille</th>
<th scope="col" class="org-left">sans threads</th>
<th scope="col" class="org-left">avec threads</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">att48\_d.tsp</td>
<td class="org-right">48</td>
<td class="org-left">0m1,655s</td>
<td class="org-left">0m1,208s</td>
</tr>


<tr>
<td class="org-left">berlin52.tsp</td>
<td class="org-right">52</td>
<td class="org-left">0m2,021s</td>
<td class="org-left">0m1,372s</td>
</tr>


<tr>
<td class="org-left">dantzig42\_d.txt</td>
<td class="org-right">42</td>
<td class="org-left">0m1,166s</td>
<td class="org-left">0m0,914s</td>
</tr>


<tr>
<td class="org-left">ex1.tsp</td>
<td class="org-right">4</td>
<td class="org-left">0m0,014s</td>
<td class="org-left">0m0,027s</td>
</tr>


<tr>
<td class="org-left">ex2.tsp</td>
<td class="org-right">4</td>
<td class="org-left">0m0,017s</td>
<td class="org-left">0m0,030s</td>
</tr>


<tr>
<td class="org-left">ex3.tsp</td>
<td class="org-right">5</td>
<td class="org-left">0m0,017s</td>
<td class="org-left">0m0,030s</td>
</tr>


<tr>
<td class="org-left">ex4.tsp</td>
<td class="org-right">5</td>
<td class="org-left">0m0,023s</td>
<td class="org-left">0m0,034s</td>
</tr>


<tr>
<td class="org-left">ex5.tsp</td>
<td class="org-right">3</td>
<td class="org-left">0m0,012s</td>
<td class="org-left">0m0,024s</td>
</tr>


<tr>
<td class="org-left">five\_d.txt</td>
<td class="org-right">5</td>
<td class="org-left">0m0,018s</td>
<td class="org-left">0m0,029s</td>
</tr>


<tr>
<td class="org-left">fri26\_d.txt</td>
<td class="org-right">26</td>
<td class="org-left">0m0,374s</td>
<td class="org-left">0m0,344s</td>
</tr>


<tr>
<td class="org-left">gr17\_d.txt</td>
<td class="org-right">17</td>
<td class="org-left">0m0,157s</td>
<td class="org-left">0m0,168s</td>
</tr>


<tr>
<td class="org-left">p01\_d.txt</td>
<td class="org-right">15</td>
<td class="org-left">0m0,117s</td>
<td class="org-left">0m0,130s</td>
</tr>


<tr>
<td class="org-left">wg59\_dist.txt</td>
<td class="org-right">59</td>
<td class="org-left">0m2,852s</td>
<td class="org-left">0m1,800s</td>
</tr>
</tbody>
</table>

On remarque qu'ici l'execution san thread et plus efficaces pour les
datasets contenant moins 26 villes.

-   `cycles = 3, fourmis = 100`

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">dataset</th>
<th scope="col" class="org-right">taille</th>
<th scope="col" class="org-left">sans threads</th>
<th scope="col" class="org-left">avec threads</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">att48\_d.tsp</td>
<td class="org-right">48</td>
<td class="org-left">0m4,925s</td>
<td class="org-left">0m3,671s</td>
</tr>


<tr>
<td class="org-left">berlin52.tsp</td>
<td class="org-right">52</td>
<td class="org-left">0m6,045s</td>
<td class="org-left">0m4,335s</td>
</tr>


<tr>
<td class="org-left">dantzig42\_d.txt</td>
<td class="org-right">42</td>
<td class="org-left">0m3,880s</td>
<td class="org-left">0m5,607s</td>
</tr>


<tr>
<td class="org-left">ex1.tsp</td>
<td class="org-right">4</td>
<td class="org-left">0m0,044s</td>
<td class="org-left">0m0,246s</td>
</tr>


<tr>
<td class="org-left">ex2.tsp</td>
<td class="org-right">4</td>
<td class="org-left">0m0,042s</td>
<td class="org-left">0m0,087s</td>
</tr>


<tr>
<td class="org-left">ex3.tsp</td>
<td class="org-right">5</td>
<td class="org-left">0m0,038s</td>
<td class="org-left">0m0,083s</td>
</tr>


<tr>
<td class="org-left">ex4.tsp</td>
<td class="org-right">5</td>
<td class="org-left">0m0,064s</td>
<td class="org-left">0m0,111s</td>
</tr>


<tr>
<td class="org-left">ex5.tsp</td>
<td class="org-right">3</td>
<td class="org-left">0m0,025s</td>
<td class="org-left">0m0,059s</td>
</tr>


<tr>
<td class="org-left">five\_d.txt</td>
<td class="org-right">5</td>
<td class="org-left">0m0,039s</td>
<td class="org-left">0m0,095s</td>
</tr>


<tr>
<td class="org-left">fri26\_d.txt</td>
<td class="org-right">26</td>
<td class="org-left">0m1,267s</td>
<td class="org-left">0m1,663s</td>
</tr>


<tr>
<td class="org-left">gr17\_d.txt</td>
<td class="org-right">17</td>
<td class="org-left">0m0,526s</td>
<td class="org-left">0m0,652s</td>
</tr>


<tr>
<td class="org-left">p01\_d.txt</td>
<td class="org-right">15</td>
<td class="org-left">0m0,353s</td>
<td class="org-left">0m0,376s</td>
</tr>


<tr>
<td class="org-left">wg59\_dist.txt</td>
<td class="org-right">59</td>
<td class="org-left">0m8,975s</td>
<td class="org-left">0m6,508s</td>
</tr>
</tbody>
</table>

Dans ce cas on remarque aussi ce qu'on avait dit auparavant.


<a id="orgb58ccfb"></a>

## Approximation de la solution

Dans les examples ci-dessous `tspant` arrive à donner la solution
optimale connue pour tous les problèmes sauf pour `berlin52`,
`wg59` ce qui montre la difficulté pour travailler avec des grands
réseaux de villes. Cependant cela peut être dû aux règles prises
dans ce projet. Puisque pour avoir des solutions optimales il faut
adapter les équations en ajoutant des heuristiques et des
paramètres adaptés au réseau. Le but de ce projet n'étant pas de
les trouver, on se contentera de pouvoir résoudre optimalement des
réseaux de villes simples.

    Règle 1: Aller vers la ville (i,j) avec une probabilité p_ij/d_ij et si elle
    n'existe pas dans la liste "tabou".
    
    Règle 2: p_ij(t+s) = p_ij/2(t) + (nb de fourmis par ville) * (nb de fourmis
    passées entre i et j).


<a id="org4e0eac8"></a>

# Conclusion

Ce projet nous a permis de pouvoir résoudre optimalement des réseaux
de villes simples. Cependant il pourrait nous donner des meilleurs
résultats si on adapterait les équations de transitions.

La multitâche ajouté au projet nous permet de gagner un peu de temps
de calculs mais ce n'est pas très important. Si on faisait face à
des réseau de villes gigantèsques peut être il faudrait chercher une
solution avec MPI car ce programme n'est pas forcément adapter pour
le genre de réseaux. Mais déjà on arrive à avoir des solutions bien
meilleurs que d'autres algorithmes du PVC.


<a id="org60beed7"></a>

# Datasets


<a id="org8f1fbd0"></a>

## Données

Les matrices de distance ont été retrouvés du site:
<https://people.sc.fsu.edu/~jburkardt/datasets/tsp/tsp.html>


<a id="orge02c716"></a>

## Solutions

<https://www.iwr.uni-heidelberg.de/groups/comopt/software/TSPLIB95/STSP.html>


